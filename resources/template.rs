pub trait Scan {
  fn scan<'a, I>(eles: &mut I) -> Self where I: Iterator<Item=&'a str>;
}

macro_rules! impl_scan_single {
  ($ty:ty) => {
    impl Scan for $ty {
      fn scan<'a, I>(eles: &mut I) -> Self where I: Iterator<Item=&'a str> {
        let next = eles.next().unwrap();
        match next.parse() {
          Ok(res) => res,
          Err(_) => panic!()
        }
      }
    }
  }
}

impl_scan_single!(u8);
impl_scan_single!(u16);
impl_scan_single!(u32);
impl_scan_single!(u64);
impl_scan_single!(usize);
impl_scan_single!(i8);
impl_scan_single!(i16);
impl_scan_single!(i32);
impl_scan_single!(i64);
impl_scan_single!(isize);
impl_scan_single!(f32);
impl_scan_single!(f64);
impl_scan_single!(String);

const __IMPL_SCAN_FOR_VEC: () = {
  use std::str::FromStr;
  impl<T> Scan for Vec<T> where T: FromStr {
    fn scan<'a, I>(eles: &mut I) -> Self where I: Iterator<Item=&'a str> {
      eles.map(|str| match str.parse() {
        Ok(res) => res,
        Err(_) => panic!()
      }).collect()
    }
  }
};

// -----------------------------------------------------------------------------

fn main() {
  #[allow(unused_imports)]
  use std::io::{self, BufReader, BufRead, Write, BufWriter};
  let __stdin = io::stdin();
  let __stdout = io::stdout();
  let mut __in = BufReader::with_capacity(4096, __stdin.lock());
  let mut __buf = String::new();
  let mut __out = BufWriter::with_capacity(4096, __stdout.lock());

  macro_rules! scanln {
    ($($var:ident : $ty:path),+) => {
      $(let $var: $ty;)+
      {
        __buf.clear();
        __in.read_line(&mut __buf).unwrap();
        let mut eles = __buf.trim().split_whitespace();
        $($var = Scan::scan(&mut eles);)+
      }
    }
  }

  macro_rules! put {
    ($fmt:expr) => { write!(__out, $fmt).unwrap(); };
    ($fmt:expr, $($arg:expr),+) => { write!(__out, $fmt, $($arg),+).unwrap(); }
  }
  macro_rules! putln {
    () => { writeln!(__out, "").unwrap(); };
    ($fmt:expr) => { writeln!(__out, $fmt).unwrap(); };
    ($fmt:expr, $($arg:expr),+) => { writeln!(__out, $fmt, $($arg),+).unwrap(); }
  }
  macro_rules! fflush {
    () => { __out.flush().unwrap(); }
  }

  // -- PLACE SOLUTION BODY BELOW THIS LINE ------------------------------------
}
