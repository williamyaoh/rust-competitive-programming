pub mod random {
  //! Generating random integers quickly without using the `rand` crate.
  //! Not cryptographically secure.

  pub trait Generator {
    fn gen(&mut self) -> u64;
  }

  /// [xorshift128+](https://en.wikipedia.org/wiki/Xorshift#xorshift.2B)
  pub struct XS128P {
    pub s1: u64,
    pub s2: u64
  }

  impl XS128P {
    pub fn new() -> Self {
      XS128P {
        // These values are chosen arbitrarily. They just need to not be 0.
        s1: 0x8efd400187ee7378,
        s2: 0x5fea0866650106ab
      }
    }
  }

  impl Generator for XS128P {
    fn gen(&mut self) -> u64 {
      let mut x = self.s1;
      let y = self.s2;
      self.s1 = y;
      x ^= x << 23;;
      self.s2 = x ^ y ^ (x >> 17) ^ (y >> 26);
      self.s2.wrapping_add(y)
    }
  }
}

// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
  use ::random::{XS128P, Generator};

  #[test]
  fn test_dieroll() {
    let mut gen = XS128P::new();
    let mut freq = vec![0; 6];

    for _ in 0..100_000 {
      let rand = gen.gen() % 6;
      freq[rand as usize] += 1;
    }

    let expected = 100_000f64 / 6f64;

    for f in freq {
      let f = f as f64;
      let diff = expected - f;
      assert!(diff / expected < 0.02);
    }
  }
}
