pub mod roots {
  //! Methods for computing roots that are faster than using floating-point
  //! arithmetic or binary search.

  /// Integer cube root. Returns the smallest integer `y` such
  /// that `y * y * y <= x`.
  pub fn icbrt64(mut x: u64) -> u32 {
    let mut s: i32;
    let mut y: u32 = 0;
    let mut b: u64;

    s = 63;
    while s >= 0 {
      y += y;
      b = 3 * (y as u64) * (y as u64 + 1) + 1;
      if x >> s >= b {
        x -= b << s;
        y += 1;
      }
      s -= 3;
    }
    y
  }
}

// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
  use ::roots::icbrt64;
  use ::random::{Generator, XS128P};

  #[test]
  fn test_basic_cubic_root() {
    assert!(icbrt64(8) == 2);
    assert!(icbrt64(27) == 3);
    assert!(icbrt64(64) == 4);
    assert!(icbrt64(125) == 5);
    assert!(icbrt64(216) == 6);
  }

  #[test]
  fn test_random_cubic_root() {
    let mut rand = XS128P::new();

    for _ in 0..1_000_000 {
      let x = rand.gen() % 10_000_000_000_000_000u64;
      let rt = icbrt64(x) as u64;
      let rtp1 = rt + 1;
      assert!(rt * rt * rt <= x);
      assert!(rtp1 * rtp1 * rtp1 > x);
    }
  }
}
