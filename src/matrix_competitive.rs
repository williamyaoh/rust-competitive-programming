pub mod matrix {
  //! 2D matrix of elements.
  //! Easier to initialize than a vector of vectors, and
  //! has better cache locality.

  /// Stores elements in row-major order.
  /// The *first* index is the one with adjacent elements.
  pub struct Matrix<T> {
    pub elements: Vec<T>,
    pub width: usize
  }

  impl<T> Matrix<T> {
    pub fn row_major_index(&self, idx: (usize, usize)) -> usize {
      let (x, y) = idx;
      y * self.width + x
    }

    pub fn get(&self, idx: (usize, usize)) -> Option<&T> {
      let index = self.row_major_index(idx);
      if index < self.elements.len() {
        Some(&self.elements[index])
      } else {
        None
      }
    }

    pub fn get_mut(&mut self, idx: (usize, usize)) -> Option<&mut T> {
      let index = self.row_major_index(idx);
      if index < self.elements.len() {
        Some(&mut self.elements[index])
      } else {
        None
      }
    }
  }

  impl<T> Matrix<T> where T: Default + Clone {
    pub fn new(width: usize, height: usize) -> Self {
      Matrix {
        elements: vec![T::default(); width * height],
        width: width
      }
    }
  }

  impl<T> Matrix<T> where T: Clone {
    pub fn with_default(width: usize, height: usize, default: T) -> Self {
      Matrix {
        elements: vec![default; width * height],
        width: width
      }
    }
  }

  const __IMPL_INDEX_FOR_MATRIX: () = {
    use std::ops::{Index, IndexMut};

    impl<T> Index<(usize, usize)> for Matrix<T> {
      type Output = T;
      fn index(&self, idx: (usize, usize)) -> &Self::Output {
        self.get(idx).unwrap()
      }
    }

    impl<T> IndexMut<(usize, usize)> for Matrix<T> {
      fn index_mut(&mut self, idx: (usize, usize)) -> &mut Self::Output {
        self.get_mut(idx).unwrap()
      }
    }
  };
}

// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
  use ::matrix::Matrix;
  use ::random::{Generator, XS128P};

  #[test]
  fn test_valid_index() {
    let mut rand = XS128P::new();
    let width = (rand.gen() % 500) as usize;
    let height = (rand.gen() % 500) as usize;

    let mat: Matrix<u32> = Matrix::with_default(width, height, 0xDEADBEEF);

    for j in 0..height {
      for i in 0..width {
        let x = mat[(i, j)];
        assert!(x == 0xDEADBEEF);
      }
    }
  }
}
