//! This file doesn't actually do anything; it's just here so that
//! cargo will see all our files and run their tests.
//!
//! See the `README` for why we organize the library this way i.e.
//! putting a separate module inside each module.

mod ascii_competitive;
mod matrix_competitive;
mod random_competitive;
mod roots_competitive;
mod monoid_competitive;
mod vector_competitive;
mod segment_tree_competitive;

pub use ascii_competitive::ascii;
pub use matrix_competitive::matrix;
pub use random_competitive::random;
pub use roots_competitive::roots;
pub use monoid_competitive::monoid;
pub use vector_competitive::vector;
pub use segment_tree_competitive::segment_tree;
