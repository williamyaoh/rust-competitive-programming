pub mod ascii {
  //! For string-processing problems, it's useful to be able to treat
  //! `u8`s directly as characters. The `CharExt` trait provides this,
  //! allowing direct method calls for various character-based functions
  //! on `u8`.
  //!
  //! Don't forget to import `CharExt` when using this module.

  pub trait CharExt: Copy {
    fn is_digit(self, radix: u32) -> bool;
    fn to_digit(self, radix: u32) -> Option<u32>;
    fn is_alphabetic(self) -> bool;
    fn is_lowercase(self) -> bool;
    fn is_uppercase(self) -> bool;
    fn is_whitespace(self) -> bool;
    fn is_alphanumeric(self) -> bool;
    fn to_lowercase(self) -> Self;
    fn to_uppercase(self) -> Self;
  }

  macro_rules! impl_u8 {
    ($meth:ident($($arg:ident : $ty:ty),*) -> $ret:ty) => {
      fn $meth(self, $($arg : $ty),*) -> $ret {
        let c = self as char;
        c.$meth($($arg),*)
      }
    }
  }

  impl CharExt for u8 {
    impl_u8!(is_digit(radix: u32) -> bool);
    impl_u8!(to_digit(radix: u32) -> Option<u32>);
    impl_u8!(is_alphabetic() -> bool);
    impl_u8!(is_lowercase() -> bool);
    impl_u8!(is_uppercase() -> bool);
    impl_u8!(is_whitespace() -> bool);
    impl_u8!(is_alphanumeric() -> bool);
    fn to_lowercase(self) -> Self {
      let c = self as char;
      c.to_lowercase()
        .next()
        .map(|c| c as u8)
        .unwrap_or(self)
    }
    fn to_uppercase(self) -> Self {
      let c = self as char;
      c.to_uppercase()
        .next()
        .map(|c| c as u8)
        .unwrap_or(self)
    }
  }
}

// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
  use ::ascii::CharExt;

  #[test]
  fn test_is_digit() {
    for i in 0..10 {
      let c = b'0' + (i as u8);
      assert!(c.is_digit(10));
    }
  }

  #[test]
  fn test_is_hex_digit() {
    let digits = [b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9',
                  b'a', b'b', b'c', b'd', b'e', b'f'];
    for c in digits.iter() {
      assert!(c.is_digit(16));
    }
  }

  #[test]
  fn test_is_not_digit() {
    for i in 0..26 {
      let c = b'a' + (i as u8);
      assert!(!c.is_digit(10));
    }
    for i in 0..26 {
      let c = b'A' + (i as u8);
      assert!(!c.is_digit(10));
    }
  }

  #[test]
  fn test_to_digit() {
    for i in 0..10 {
      let c = b'0' + (i as u8);
      let digit = c.to_digit(10);
      assert!(digit.is_some());
      assert!(digit.unwrap() == i as u32);
    }
  }

  #[test]
  fn test_to_hex_digit() {
    let digits = [b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9',
                  b'a', b'b', b'c', b'd', b'e', b'f'];
    for i in 0..16 {
      let c = digits[i];
      let digit = c.to_digit(16);
      assert!(digit.is_some());
      assert!(digit.unwrap() == i as u32);
    }
  }

  #[test]
  fn test_to_digit_fail() {
    for i in 0..26 {
      let c = b'a' + (i as u8);
      let digit = c.to_digit(10);
      assert!(digit.is_none());
    }
  }

  #[test]
  fn test_alphabetic() {
    for i in 0..26 {
      let c = b'a' + (i as u8);
      assert!(c.is_alphabetic());
    }
    for i in 0..26 {
      let c = b'A' + (i as u8);
      assert!(c.is_alphabetic());
    }
  }

  #[test]
  fn test_not_alphabetic() {
    for i in 0..10 {
      let c = b'0' + (i as u8);
      assert!(!c.is_alphabetic());
    }
  }

  #[test]
  fn test_lowercase() {
    for i in 0..26 {
      let c = b'a' + (i as u8);
      assert!(c.is_lowercase());
      assert!(!c.is_uppercase());
    }
  }

  #[test]
  fn test_uppercase() {
    for i in 0..26 {
      let c = b'A' + (i as u8);
      assert!(!c.is_lowercase());
      assert!(c.is_uppercase());
    }
  }

  #[test]
  fn test_only_one_case() {
    for c in 0..256 {
      let c = c as u8;
      assert!(!(c.is_lowercase() && c.is_uppercase()));
    }
  }

  #[test]
  fn test_whitespace() {
    let c = b' ';
    assert!(c.is_whitespace());
    for i in 0..26 {
      let c = b'a' + (i as u8);
      assert!(!c.is_whitespace());
    }
  }

  #[test]
  fn test_alphanumeric() {
    for i in 0..10 {
      let c = b'0' + (i as u8);
      assert!(c.is_alphanumeric());
    }
    for i in 0..26 {
      let c = b'a' + (i as u8);
      assert!(c.is_alphanumeric());
    }
    for i in 0..26 {
      let c = b'A' + (i as u8);
      assert!(c.is_alphanumeric());
    }
  }

  #[test]
  fn test_alphanumeric_implication() {
    fn implies(a: bool, b: bool) -> bool { !a || b }

    for i in 0..256 {
      let c = i as u8;
      assert!(implies(c.is_digit(10), c.is_alphanumeric()));
      assert!(implies(c.is_alphabetic(), c.is_alphanumeric()));
      assert!(implies(c.is_alphanumeric(),
                      c.is_digit(10) || c.is_alphabetic()));
    }
  }

  #[test]
  fn test_casing_identity() {
    for i in 0..10 {
      let c = b'0' + (i as u8);
      assert!(c == c.to_lowercase());
      assert!(c == c.to_uppercase());
    }
  }

  #[test]
  fn test_casing() {
    for i in 0..26 {
      let lower = b'a' + (i as u8);
      let upper = b'A' + (i as u8);
      assert!(lower.to_uppercase() == upper);
      assert!(upper.to_lowercase() == lower);
    }
  }
}
