pub mod vector {
  //! Some useful methods for vectors which aren't provided in `std`.

  pub trait Vector<T> {
    fn push_index(&mut self, element: T) -> usize;
  }

  impl<T> Vector<T> for Vec<T> {
    fn push_index(&mut self, element: T) -> usize {
      let index = self.len();
      self.push(element);
      index
    }
  }
}

// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
  use ::vector::Vector;
  use ::random::{XS128P, Generator};

  #[test]
  fn test_insert_only() {
    let mut arr = vec![];
    let mut lookup = vec![];
    let mut rand = XS128P::new();

    for _ in 0..100_000 {
      let num = rand.gen();
      let i = arr.push_index(num);
      lookup.push((i, num));
    }

    for (i, num) in lookup {
      assert!(arr[i] == num);
    }
  }

  #[test]
  fn test_insert_delete() {
    use ::std::collections::HashMap;

    let mut arr = vec![];
    let mut lookup = HashMap::new();
    let mut rand = XS128P::new();

    for _ in 0..100_000 {
      let num = rand.gen();
      let coin = rand.gen() % 2;

      if coin == 0 {
        let i = arr.push_index(num);
        lookup.insert(i, num);
      } else {
        arr.pop();
      }
    }

    for (i, num) in lookup {
      if let Some(&mun) = arr.get(i) {
        assert!(num == mun);
      }
    }
  }
}
