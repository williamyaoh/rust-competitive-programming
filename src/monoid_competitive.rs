pub mod monoid {
  //! Common binary operations: addition, min, max, in an abstracted way.

  pub trait Semigroup {
    /// **A**ssociative **B**inary **O**perator.
    fn abo(left: Self, right: Self) -> Self;
  }
  pub trait Monoid: Semigroup {
    fn identity() -> Self;
  }

  #[derive(Clone, Copy, Debug)]
  #[derive(PartialEq, Eq, PartialOrd, Ord)]
  pub struct Sum<T>(pub T);
  #[derive(Clone, Copy, Debug)]
  #[derive(PartialEq, Eq, PartialOrd, Ord)]
  pub struct Min<T>(pub T);
  #[derive(Clone, Copy, Debug)]
  #[derive(PartialEq, Eq, PartialOrd, Ord)]
  pub struct Max<T>(pub T);

  macro_rules! impl_simple_sum {
    ($ty:ty) => {
      impl Semigroup for Sum<$ty> {
        fn abo(left: Self, right: Self) -> Self {
          Sum(left.0 + right.0)
        }
      }

      impl Monoid for Sum<$ty> {
        fn identity() -> Self {
          Sum(0 as $ty)
        }
      }
    }
  }

  impl_simple_sum!(i8);
  impl_simple_sum!(i16);
  impl_simple_sum!(i32);
  impl_simple_sum!(i64);
  impl_simple_sum!(isize);
  impl_simple_sum!(u8);
  impl_simple_sum!(u16);
  impl_simple_sum!(u32);
  impl_simple_sum!(u64);
  impl_simple_sum!(usize);
  impl_simple_sum!(f32);
  impl_simple_sum!(f64);

  macro_rules! impl_simple_min {
    ($ty:ident) => {
      impl Semigroup for Min<$ty> {
        fn abo(left: Self, right: Self) -> Self {
          Min(::std::cmp::min(left.0, right.0))
        }
      }

      impl Monoid for Min<$ty> {
        fn identity() -> Self {
          Min(::std::$ty::MAX)
        }
      }
    }
  }

  impl_simple_min!(i8);
  impl_simple_min!(i16);
  impl_simple_min!(i32);
  impl_simple_min!(i64);
  impl_simple_min!(isize);
  impl_simple_min!(u8);
  impl_simple_min!(u16);
  impl_simple_min!(u32);
  impl_simple_min!(u64);
  impl_simple_min!(usize);

  macro_rules! impl_simple_max {
    ($ty:ident) => {
      impl Semigroup for Max<$ty> {
        fn abo(left: Self, right: Self) -> Self {
          Max(::std::cmp::max(left.0, right.0))
        }
      }

      impl Monoid for Max<$ty> {
        fn identity() -> Self {
          Max(::std::$ty::MIN)
        }
      }
    }
  }

  impl_simple_max!(i8);
  impl_simple_max!(i16);
  impl_simple_max!(i32);
  impl_simple_max!(i64);
  impl_simple_max!(isize);
  impl_simple_max!(u8);
  impl_simple_max!(u16);
  impl_simple_max!(u32);
  impl_simple_max!(u64);
  impl_simple_max!(usize);
}

// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
  use ::monoid::{Semigroup, Monoid, Sum, Min, Max};
  use ::random::{Generator, XS128P};

  #[test]
  fn test_additive_identity() {
    let identity = Sum::<u64>::identity();
    let mut rand = XS128P::new();
    for _ in 0..10_000 {
      let num = Sum(rand.gen());
      let combine1 = Sum::abo(identity, num);
      let combine2 = Sum::abo(num, identity);
      assert!(combine1 == num);
      assert!(combine2 == num);
    }
  }

  #[test]
  fn test_min_identity() {
    let identity = Min::<u64>::identity();
    let mut rand = XS128P::new();
    for _ in 0..10_000 {
      let num = Min(rand.gen());
      let combine1 = Min::abo(identity, num);
      let combine2 = Min::abo(num, identity);
      assert!(combine1 == num);
      assert!(combine2 == num);
    }
  }

  #[test]
  fn test_max_identity() {
    let identity = Max::<u64>::identity();
    let mut rand = XS128P::new();
    for _ in 0..10_000 {
      let num = Max(rand.gen());
      let combine1 = Max::abo(identity, num);
      let combine2 = Max::abo(num, identity);
      assert!(combine1 == num);
      assert!(combine2 == num);
    }
  }
}
