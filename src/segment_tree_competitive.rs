pub mod segment_tree {
  //! A structure which allows range queries of a semigroup over contiguous ranges,
  //! as well as point updates and range updates. The point and range updates do
  //! not need to form the same semigroup as the querying semigroup.

  use ::monoid::Semigroup;

  type ChildPtr = Option<usize>;

  pub struct SegmentTree<T> {
    pub elements: Vec<T>,
    pub children: Vec<(ChildPtr, ChildPtr)>,
    pub range: Vec<(usize, usize)>,
    pub update: Vec<Option<T>>,
    pub len_orig: usize,
    pub root: usize
  }

  impl<T> SegmentTree<T> where T: Semigroup + Copy {
    pub fn new(data: &[T]) -> Self {
      assert!(data.len() != 0, "cannot create segment tree from empty array");

      let mut st = SegmentTree {
        elements: vec![],
        children: vec![],
        range: vec![],
        update: vec![],
        len_orig: data.len(),
        root: 0
      };

      /// `rs` = range start, `re` = range end
      fn construct<T>(st: &mut SegmentTree<T>, data: &[T], rs: usize, re: usize)
        -> usize where
          T: Semigroup + Copy
      {
        use ::vector::Vector;

        if rs == re {
          st.update.push(None);
          st.children.push((None, None));
          st.range.push((rs, re));
          st.elements.push_index(data[rs])
        } else {
          let mid = (rs + re) / 2;  // TODO: fix potential overflow
          let l = construct(st, data, rs, mid);
          let r = construct(st, data, mid + 1, re);
          let m = T::abo(st.elements[l], st.elements[r]);
          st.update.push(None);
          st.children.push((Some(l), Some(r)));
          st.range.push((rs, re));
          st.elements.push_index(m)
        }
      }

      let root = construct(&mut st, data, 0, data.len() - 1);
      st.root = root;

      st
    }

    pub fn query(&self, rs: usize, re: usize) -> T {
      assert!(rs < self.len_orig);
      assert!(re < self.len_orig);
      
      fn query_node<T>(st: &SegmentTree<T>, here: usize, rs: usize, re: usize)
        -> T
          where T: Semigroup + Copy
      {
        let (start, end) = st.range[here];
        if rs == start && re == end {
          return st.elements[here];
        }
        let mid = (start + end) / 2;  // TODO: fix potential overflow
        let (left, right) = st.children[here];
        let (left, right) = (left.unwrap(), right.unwrap());
        if rs <= mid && re > mid {
          T::abo(query_node(st, left, rs, mid), query_node(st, right, mid + 1, re))
        } else if re <= mid {
          query_node(st, left, rs, re)
        } else {
          query_node(st, right, rs, re)
        }
      }

      query_node(self, self.root, rs, re)
    }
  }
}

// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
  use ::segment_tree::SegmentTree;
  use ::monoid::{Sum, Min};
  use ::random::{XS128P, Generator};

  #[test]
  fn test_summing() {
    let mut rand = XS128P::new();
    let mut elements = Vec::with_capacity(10_000);

    for _ in 0..10_000 {
      elements.push(rand.gen() % 100);
    }

    let sums: Vec<Sum<u64>> = elements.iter()
      .map(|&n| Sum(n))
      .collect();

    let st = SegmentTree::new(&sums);

    fn sum_range(elements: &[u64], rs: usize, re: usize) -> u64 {
      let mut res = 0;
      for i in rs..re+1 {
        res += elements[i];
      }
      res
    }

    for _ in 0..2_000 {
      let mut rs = (rand.gen() % 10_000) as usize;
      let mut re = (rand.gen() % 10_000) as usize;
      if rs > re { ::std::mem::swap(&mut rs, &mut re); }
      assert_eq!(
        sum_range(&elements, rs, re),
        st.query(rs, re).0
      );
    }
  }

  #[test]
  fn test_min() {
    let mut rand = XS128P::new();
    let mut elements = Vec::with_capacity(10_000);

    for _ in 0..10_000 {
      elements.push(rand.gen());
    }

    let mins: Vec<Min<u64>> = elements.iter()
      .map(|&n| Min(n))
      .collect();

    let st = SegmentTree::new(&mins);

    fn min_range(elements: &[u64], rs: usize, re: usize) -> u64 {
      let mut res = u64::max_value();
      for i in rs..re+1 {
        res = ::std::cmp::min(res, elements[i]);
      }
      res
    }

    for _ in 0..2_000 {
      let mut rs = (rand.gen() % 10_000) as usize;
      let mut re = (rand.gen() % 10_000) as usize;
      if rs > re { ::std::mem::swap(&mut rs, &mut re); }
      assert_eq!(
        min_range(&elements, rs, re),
        st.query(rs, re).0
      );
    }
  }
}
